<?php

namespace Drupal\krumo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for passing-through JS and image assets belonging to Krumo.
 */
class AssetController extends ControllerBase {

  /**
   * Returns an image for a given Krumo skin.
   *
   * @param string $skin
   *   The name of the skin.
   * @param string $img
   *   The name of the image.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function image(string $skin, string $img): Response {
    $filename = _krumo_get_base_dir() . "/skins/$skin/$img.gif";
    if (is_readable($filename)) {
      $response = new Response();
      $response->setContent(file_get_contents($filename));
      $response->headers->set('content-type', 'image/gif');
      $response->headers->set('content-length', filesize($filename));
      return $response;
    }
    throw new FileNotFoundException("/krumo/$skin/$img.gif");
  }

  /**
   * Returns minified krumo.js.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function krumoJs(): Response {
    $filename = _krumo_get_base_dir() . '/js/krumo.min.js';
    if (is_readable($filename)) {
      $response = new Response();
      $response->setContent(file_get_contents($filename));
      $response->headers->set('content-type', 'text/javascript');
      $response->headers->set('content-length', filesize($filename));
      return $response;
    }
    throw new FileNotFoundException("/krumo/krumo.js");
  }

}