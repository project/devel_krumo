<?php

namespace Drupal\krumo\Plugin\Devel\Dumper;

use Drupal\devel\DevelDumperBase;
use Drupal\krumo\KrumoWrapper;

/**
 * Provides a Krumo dumper plugin.
 *
 * @DevelDumper(
 *   id = "krumo",
 *   label = @Translation("Krumo"),
 *   description = @Translation("Wrapper for Krumo, a debugging tool."),
 * )
 */
class Krumo extends DevelDumperBase {

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL) {
    return $this->setSafeMarkup(KrumoWrapper::dumpAndReturn($input));
  }

  /**
   * {@inheritdoc}
   */
  public static function checkRequirements(): bool {
    return TRUE;
  }

}
