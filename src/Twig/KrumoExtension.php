<?php

namespace Drupal\krumo\Twig;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\krumo\KrumoWrapper;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\Template;
use Twig\TwigFunction;

/**
 * Provides the Krumo debugging function within Twig templates.
 */
class KrumoExtension extends AbstractExtension {

  protected bool $mayAccess;

  public function __construct(AccountProxyInterface $account) {
    $this->mayAccess = $account->hasPermission('access krumo');
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new TwigFunction('krumo', $this->krumo(...), [
        'is_safe' => ['html'],
        'needs_environment' => TRUE,
        'needs_context' => TRUE,
        'is_variadic' => TRUE,
      ]),
    );
  }

  /**
   * Provides Krumo function to Twig templates.
   *
   * Handles 0, 1, or multiple arguments.
   *
   * Code derived from https://github.com/barelon/CgKintBundle.
   *
   * @param \Twig\Environment $env
   *   The twig environment instance.
   * @param array $context
   *   An array of parameters passed to the template.
   * @param array $args
   *   An array of parameters passed the function.
   *
   * @return string
   *   String representation of the input variables.
   */
  public function krumo(Environment $env, array $context, array $args = []): string {
    // Display nothing to the unauthorized.
    if (!$this->mayAccess) {
      return '';
    }
    // Don't do anything unless twig_debug is enabled. This reads from the Twig
    // environment, not Drupal Settings, so a container rebuild is necessary
    // when toggling twig_debug on and off. We can consider injecting Settings.
    if (!$env->isDebug()) {
      return '';
    }

    // No arguments passed to krumo(), display full Twig context.
    if (empty($args)) {
      $krumo_variable = [];
      foreach ($context as $key => $value) {
        if (!$value instanceof Template) {
          $krumo_variable[$key] = $value;
        }
      }
      $output = KrumoWrapper::dumpAndReturn($krumo_variable);
    }
    else {
      $output = call_user_func_array([KrumoWrapper::class, 'dumpAndReturn'], $args);
    }

    return $output;
  }

}
