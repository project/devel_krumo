<?php

namespace Drupal\krumo;

use Drupal\Component\Utility\Html;

/**
 * Wrapper around Krumo so that its assets are publicly accessible.
 *
 * Krumo's images, JS, and CSS are hidden in the vendor dir. This wrapper
 * adjusts output so that they point to routed paths that can then pass the
 * required data through.
 */
class KrumoWrapper extends \Krumo {

  /**
   * Overrides parent get_icon method so we can mangle the icon path.
   */
  public static function get_icon($name, $title) {
    return '<img style="padding:0 2px" src="/krumo/icons/' . Html::escape($name) . '" title="' . Html::escape($title) . '" alt="' . Html::escape($name) . '">';
  }

  /**
   * Gets the output of Krumo & massages it so that asset paths are accessible.
   *
   * @param mixed $data
   * @param mixed $second
   *
   * @return string
   */
  public static function dumpAndReturn($data, $second = ''): string {
    ob_start();
    self::dump($data, $second);
    $output = ob_get_clean();

    // This crap doesn't belong here.
    $output = str_replace('<!DOCTYPE html>', '', $output);

    // Clean up CSS.
    if (preg_match('~<!-- Using Krumo Skin: "([^"]+)".*<!-- Krumo - CSS -->~Uims', $output, $matches)) {
      $skin = $matches[1];
      if (is_file($filename = _krumo_get_base_dir() . "/skins/$skin/skin.css")) {
        $css = str_replace('%url%', "/krumo/skins/$skin/", file_get_contents($filename));
        $output = str_replace($matches[0], "<style>$css</style>", $output);
      }
    }
    // Clean up JS.
    if (preg_match('~<script.*<!-- Krumo - JavaScript -->~Uims', $output, $matches)) {
      $output = str_replace($matches[0], '<script src="/krumo/krumo.js"></script>', $output);
    }

    // Correct the caller info.
    if (preg_match('~Called from <strong><code>(.*)</code></strong>, line <strong><code>(.*)</code></strong>~Uims', $output, $matches)) {
      // Find the actual caller.
      $bt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
      while ($frame = array_pop($bt)) {
        $class         = strtolower($frame['class']    ?? '');
        $function      = strtolower($frame['function'] ?? '');
        $is_krumo_func = in_array($function, ['krumo', 'k', 'kd', 'dpm', 'kpr', 'dpq', 'ddm']);

        if ($is_krumo_func || $class == 'krumowrapper') {
          break;
        }
      }
      $file = $frame['file'];
      $line = $frame['line'];
      $replacements = [$matches[1] => $file, $matches[2] => $line];
      $output = strtr($output, $replacements);
    }

    return $output;
  }

}